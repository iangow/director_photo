Director photos
================

We have photos from three sources:

1.  **EDGAR**. Photos scraped from DEF 14A filings.
2.  **Scraped Equilar**. Photos scraped from links provided in data from Equilar in the `executive` schema.
3.  **Supplied Equilar**. Photos taken from links in `DirectorsForStanford-EthnicityDataProject.xlsx` file supplied by Equilar (and found in the `data` director in this repository).

Data tasks
----------

Photos from source 2 and 3 come with links to `executive_id`, Equilar's cross-firm, over-time identifier for directors and executives. Photos from source 1, however, need to be matched to `executive_id`. This matching was essentially accomplished by having a worker from [Fiverr](http://fiverr.com) match director photos with names taken from Equilar for a subject firm, by examining the underlying DEF 14A filings from which the photos came.[1]

EDGAR images
------------

Data in `edgar.filing_docs` covers all "documents" on EDGAR. Code in `classify_photos_run.py` uses the `classify_url` and `get_photo_url` functions from `classify_photos.py` to classify every photos as `is_face` or not. The results are stored in `director_photo.face_output`.

So we have 433,250 images (from 74,237 filings), of which 75,787 are classified as faces.

The next step is to select photos from these filings and, where possible, match them to Equilar, which has fairly comprehensive data on directors. As companies that include photos in proxy filings tend to do so in multiple filings, to reduce duplication of effort, we focused on the first and last filings for a given firm. We attempted to match 11,437 images (from 1,019 filings), to names using a Fiverr resource. With some back-end clean-up, we were able to match 10,273 photos to directors on Equilar (`executive_id`).

Matched photo data
------------------

Ethnic classification
---------------------

Having matched the EDGAR images to directors, we can include these with the images from Equilar and then classify images into ethnicity. The process was two-fold.

1.  Two Mechanical Turkers classify each image.
2.  When there is disagreement, either Ian Gow or Michelle Gutmann resolved the disagreement, using Internet searches and other information.

*Classification rates*

Google Sheets
-------------

There are six Google Sheets documents in the [`director_photo` directory](https://drive.google.com/drive/folders/0B_P4wvS7Nk-QQnhLQ0h4X1FHRW8?usp=sharing).

-   Fiverr data: Used by `create_photo_matches.R`.
    -   [larcker\_director\_names](https://docs.google.com/spreadsheets/d/1sdocRYWuW0T2an03ldA5VDTEZuQa4rbAckqzZCuLWhA)
    -   [larcker\_director\_names\_batch2](https://docs.google.com/spreadsheets/d/1jW4vIR8xUwFwCjQf_MWdpFlsRQ3gKDWNkFhjA7yiZew)
    -   [fuzzy\_matches.csv](https://docs.google.com/spreadsheets/d/1fAAKyHlk2MVQgjbVxIEoMNmN83lVc51-qRR3IsJoqkI)
-   [hand\_classification](https://docs.google.com/spreadsheets/d/1b7cDJy9L0VZl2sAQRr7G31LsvnRoY9YPC8GQ7ylPxZU). Manual classification of directors' ethnicities. Used by `import_hand_classification.R`.
-   [rm\_ethnicities](https://docs.google.com/spreadsheets/d/1SNDkNkkuoqb6MQuU9jR12KIlifxmyf7H1s4oOxbsFD4/edit#gid=466279335): Mapping from RiskMetrics ethnicities to standard classification.
-   [ambiguous\_rm\_matches.csv](https://docs.google.com/spreadsheets/d/1lxDIC8thAGIsnW5UZ7CFgUxbMWHcj84qbRTQt9-kBKM) used by `create_rm_equilar.R`. Used for manual disambiguation of matches from RiskMetrics to our data.

[1] One issue was that early on we only retained years and tickers for photos. We subsequently backfilled a link between photos and the underlying filing URL.
