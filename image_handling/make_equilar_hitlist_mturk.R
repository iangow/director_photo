library(dplyr, warn.conflicts = FALSE)
# PGUSER, PGPASSWORD

pg <- src_postgres()
equilar_schema <- "executive"

# Underlying data tables
photo_matches <-
    tbl(pg, sql("SELECT * FROM director_photo.photo_matches"))
equilar_photos <-
    tbl(pg, sql(paste0("SELECT * FROM director_photo.equilar_photos")))
ethnicities <-
    tbl(pg, sql("SELECT * FROM director_photo.ethnicities"))
executive <- tbl(pg, sql(paste0("SELECT * FROM ", equilar_schema,
                                ".executive")))

# Director name data (used to fill columns for MTurk upload, but not needed).
director_names <-
    executive %>%
    select(executive_id, fname, middle_name, lname, name_suffix)

# Data from EDGAR (matched by Fiverr RA)
edgar_df <-
    photo_matches %>%
    filter(!is.na(executive_id)) %>%
    rename(image_url = photo_url) %>%
    select(executive_id, director_name, image_url)

# Data from Equilar (supplied by Belen Gomez)
equilar_df <-
    equilar_photos %>%
    filter(!is.na(photo)) %>%
    rename(director_name = name,
           image_url = photo) %>%
    select(executive_id, director_name, image_url)

# Create the Equilar HIT list for MTurk
# Exclude directors we already have ethnicity for.
equilar_hitlist <-
    edgar_df %>%
    union_all(equilar_df) %>%
    anti_join(ethnicities) %>%
    left_join(director_names) %>%
    mutate(row_name = row_number()) %>%
    select(row_name, executive_id, fname, middle_name, lname,
           name_suffix, image_url, director_name) %>%
    collect()

# Note that CSV created looks just like original except that
# the first column (index in Pandas), has heading "row_name"
equilar_hitlist %>%
    write_csv("data/larcker_director_mturk-equilarHITs_v2.csv")
