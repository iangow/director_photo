import cv2
# Function to get images
import urllib.request
import matplotlib.image as mpimg
import os
import re

import numpy as np
from matplotlib import pyplot as plt

##### For Facial Recognition using OpenCV #####
#  For more information, see here: 
#     http://docs.opencv.org/trunk/d7/d8b/tutorial_py_face_detection.html
# Classifier parameters
scaleFactor = 1.2 # Parameter specifying how much the image size is reduced at each image scale
minNeighbors = 3 # Parameter specifying how many neighbors each candidate rectangle should have to retain it.

# Make face detection classifiers
xml_path = 'classifiers/'
classifer_xmls = [
     'haarcascade_frontalface_default.xml',
     'haarcascade_frontalface_alt.xml',
     'haarcascade_frontalface_alt2.xml']

# load in face classifiers
classifiers = [cv2.CascadeClassifier(xml_path + cx) 
                 for cx in classifer_xmls]
    
def classify_image(gray_img):

    if gray_img is None:
        return None
    
    try:
        for classifier in classifiers:
            faces = classifier.detectMultiScale(gray_img, scaleFactor, minNeighbors)
            if not isinstance(faces, tuple):
                return True
    
    except Exception as e:
        print(gray_img + ' failed because of error: ' + str(e))
        return None
    
    return False

def get_image(url):
    
    lfname = os.path.basename(url)
    img_exts = ['jpg','jpeg','gif','tif','tiff','bmp','png','svg']
    extension = re.search(r'.*\.(.*?)$', lfname).group(1)
    
    if extension in img_exts:
        
        try:
            lfname, headers = urllib.request.urlretrieve(url, lfname)

            # if the file is a gif file, must load it in differently
            if extension=='gif': 
                gray_img = mpimg.imread(lfname)
            # all other files are fine
            else: 
                # read the image in as grayscale (0)
                gray_img = cv2.imread(lfname, 0) 

            os.remove(lfname)
            return gray_img
        except Exception as e:
            print('URL: ' + url + ' failed because of error: ' + str(e))
            return None
        
    return None

def classify_url(url):
    gray_img = get_image(url)
    
    return classify_image(gray_img)

def get_photo_url(file_name, photo_file):
    url = re.sub(r"(-|\.txt$)", "", file_name)
    url = "https://www.sec.gov/Archives/" + url + "/" + photo_file
    return url

def show_photo(photo_url=None, gray_img=None):

    if photo_url is not None:
        gray_img = get_image(photo_url)
        print(photo_url)

    if gray_img is not None:
        plt.imshow(gray_img, cmap = 'gray', interpolation = 'bicubic')
        plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
        plt.show()
