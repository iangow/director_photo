library(RPostgreSQL)
library(dplyr, warn.conflicts = FALSE)
pg <- src_postgres()
equilar_schema <- "executive"
dbGetQuery(pg$con, "SET work_mem='5GB'")

photo_matches <-  tbl(pg, sql("SELECT * FROM director_photo.photo_matches"))

proxy_management <- tbl(pg, sql(paste0("SELECT * FROM ", equilar_schema, ".proxy_management")))
proxies <- tbl(pg, sql(paste0("SELECT * FROM ", equilar_schema, ".proxies")))
proxy_company <- tbl(pg, sql(paste0("SELECT * FROM ", equilar_schema, ".proxy_company")))
executive <- tbl(pg, sql(paste0("SELECT * FROM ", equilar_schema, ".executive")))
proxy_board_director <-
    tbl(pg, sql(paste0("SELECT * FROM ", equilar_schema, ".proxy_board_director")))

firm_years <-
    proxy_company %>%
    mutate(fy_end = to_date(concat(fiscal_year, fye), 'yyyymmdd')) %>%
    select(company_id, fiscal_year_id, fy_end) %>%
    distinct()

director_names <-
    proxy_management %>%
    select(company_id, executive_id) %>%
    union(
        proxy_board_director %>%
            select(company_id, executive_id)) %>%
    inner_join(executive) %>%
    select(-title) %>%
    inner_join(firm_years) %>%
    mutate(mname = if_else(is.na(middle_name), "", concat(middle_name, ' ')),
           suffix = if_else(is.na(name_suffix), "", concat(' ', name_suffix))) %>%
    mutate(director_name = concat(fname, ' ', mname, lname, suffix)) %>%
    select(company_id, fy_end, executive_id, director_name)

remote_image_regex <- paste0("(?:",
    "https://bytebucket.org/iangow/director_photo/raw/master/photos/edgar",
    "|https://www.sec.gov/Archives/edgar/data)")

matched_photos <-
    proxies %>%
    right_join(photo_matches) %>%
    filter(!is.na(director_name)) %>%
    left_join(director_names %>% select(-fy_end) %>% distinct()) %>%
    mutate(matched = !is.na(executive_id)) %>%
    select(executive_id, company_id, director_name, local_path, matched, name_type,
           file_name, date_filed) %>%
    compute()

matched_photos %>%
    count(matched, name_type)

unmatched <-
    matched_photos %>%
    filter(!matched, name_type!='drop-down', !is.na(company_id),
           director_name != 'NOT A PHOTO OF A PERSON')

# Confirmation that I'm correctly matching all by name that I should be.
matched_photos %>%
    filter(!matched, name_type=='drop-down',
           director_name != 'NOT A PHOTO OF A PERSON')

matched_photos %>%
    filter(!matched, name_type!='drop-down')

library(ggplot2)
library(zoo)
matched_photos %>%
    mutate(matched = concat(if_else(matched, "matched, ", "unmatched, "),
                            name_type)) %>%
    collect() %>%
    mutate(date_filed = as.yearmon(date_filed)) %>%
    ggplot(aes(x=date_filed, fill=matched)) +
    geom_histogram(binwidth=1) +
    scale_x_yearmon()


photo_matches %>%
    group_by(file_name) %>%
    filter(tagged_time > '2017-02-24 13:00:00') %>%
    summarize(tagged_time = max(tagged_time)) %>%
    collect() %>%
    ggplot(aes(x=tagged_time)) +
    geom_histogram(binwidth=3600) +
    ggtitle("Filings processed each hour")

matched_photos %>%
    filter(!matched, name_type=="write-in") %>%
    select(director_name) %>%
    distinct() %>%
    print(n=100)
