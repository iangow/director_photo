#!/usr/bin/env python3
from classify_photos import classify_url, get_photo_url

import pandas as pd

# Get data from database
import sqlalchemy as sa
import psycopg2 as pg
from pandas.io.sql import read_sql

from sqlalchemy import create_engine

engine = create_engine('postgresql://igow@iangow.me:5432/crsp')

sql = """
        SELECT file_name, document AS photo_file
        FROM edgar.filing_docs AS a
        WHERE type = 'GRAPHIC' AND document !~ '.pdf$'
            AND NOT EXISTS (
                SELECT file_name, photo_file AS document
                FROM director_photo.face_output AS b
                WHERE a.file_name = b.file_name AND a.document = b.photo_file)
        LIMIT 100
    """

import re

# Get first batch of photos to process
df = pd.read_sql(sa.text(sql), engine)
batch = 0

while True:

    batch += 1
    print("Running batch # %d" % batch, end='')

    # Compute URL for photo and then classify it.
    df['is_face'] = df.apply(lambda row: get_photo_url(row['file_name'], row['photo_file']),
                               axis=1).map(classify_url)

    # Push data to PostgreSQL database
    df.to_sql('face_output', engine, schema="director_photo",
             if_exists="append", index=False)

    if df['is_face'].count() is not None:
        print(": %d of %d images classified as faces." %
            (sum(df['is_face'].fillna(False)), df['is_face'].count()))
    else:
        print("No faces found?")

    # Get next batch of photos to process
    df = pd.read_sql(sa.text(sql), engine)

    # Break if there are no more photos
    if df['file_name'].count() == 0:
        break

engine.dispose()
