# Notes for MTurk files

First two files (Batch_2709663_batch_results.csv
and Batch_2710894_batch_results.csv) were classified with White and Male as default options in dropdown.

Batch_2718166_batch_results.csv and Batch_2718482_batch_results are for directors with Equilar photos, but no EDGAR photos.

Batch_2719682_batch_results.csv are retags from work done by WorkerIds "A224TK7J4KA1LV" and "A2S96ZZ70YFPSK".

Batch_2734387_batch_results.csv are retags for photos that previously were not in the Bitbucket repository.

Batch_2820667_batch_results.csv are tags for photos from larcker_director_mturk-equilarHITs_v2.csv.
