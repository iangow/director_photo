##### Load Modules #####
import pandas as pd

import sqlalchemy as sa
from sqlalchemy import create_engine
from pandas.io.sql import read_sql

import s3_website_defs as s_defs





##### Grab the List of Objectives #####
## Connect to PostgreSQL database
host = 'iangow.me'
user = 'mpjiang'
password = 'temp_20170213'
dbname = 'crsp'

engine = create_engine('postgresql://' + user + ':' + password + '@' + host + ':5432/' + dbname)
sql = """
    WITH faces AS (
        SELECT file_name, photo_file
        FROM director_photo.face_output
        WHERE is_face)

    SELECT * 
    FROM director_photo.filings_to_check
    INNER JOIN faces
    USING (file_name)
"""

df = pd.read_sql(sa.text(sql), engine)
df['photo_url'] = df.apply(lambda row: get_photo_url(row['file_name'], row['photo_file']),axis=1)
df['folder_name'] = df.apply(lambda row: row['file_name'].lstrip('edgar/data/').replace('/','-').rstrip('.txt'),axis=1) # folder name column
print('There are ' + str(len(df)) + ' rows')
print('There are ' + str(len(df.file_name.unique())) + ' unique documents.')

## Save Dataframe
df.to_csv('part3_master_list.csv')





###### Make a List of HTML Files to Download ######
files_dict = {}
counter = 0
for ii,filename in enumerate(df.file_name.unique()):
    print(str(ii) + '/' + str(len(df.file_name.unique())) +  ': ' + filename)
    folder_name = filename.lstrip('edgar/data/').replace('/','-').rstrip('.txt')
    sql = """
    SELECT document
    FROM director_photo.filing_docs
    WHERE file_name=\'""" + filename + """\'"""
    subset = pd.read_sql(sa.text(sql), engine)
    for jj in subset.document:
        the_file = get_photo_url(filename,jj)
        files_dict[counter] = [filename,folder_name,the_file]
        counter += 1

files_df = pd.DataFrame.from_dict(files_dict,orient='index')
files_df.columns = ['file_name','folder_name','document_url']

## Save Dataframe
files_df.to_csv('docs_download_list.csv')





###### Download Docs to IFS ######
head_path = '/usr/local/ifs/gsb/mpjiang/larcker_directorphotos/DEF_14A/part3_companies/data/'

s_defs.download_docs_to_IFS(head_path,files_df)





###### Upload Files to S3 ######
## start up S3 client and resource
s3_client = boto3.client('s3')
s3_resource = boto3.resource('s3')

s_defs.upload_files_to_S3(head_path,s3_client,s3_resource,df)





###### Pretag the Proxy Statements and Upload to S3 ######
s_defs.pretag_htmls(head_path,s3_client,df,files_df)





##### Make Google Forms and Upload to S3 #####
j_script_path = 'jquery_post_script.txt'
s_defs.make_google_forms(head_path,s3_client,df,files_df,j_script_path)





##### Make 2-Frame Displays and Upload to S3 #####
base_address = 'gsb-mpjiang-s3.s3-website-us-west-2.amazonaws.com'
s_defs.make_2frame_display(head_path,s3_client,df,files_df,base_address)





## Make List of URLs (for the Google Spreadsheet)
list_of_URLs = df[['file_name','folder_name']].copy().drop_duplicates(['file_name']).reset_index(drop=True)

base_link = 'http://' + base_address

list_of_URLs['site_link'] = list_of_URLs.apply(lambda row: base_link + row['folder_name'] + '/' +\
                                               row['folder_name'] + '_2frame_v1.html',axis=1)
list_of_URLs = list_of_URLs[['file_name','site_link']]

list_of_URLs.to_csv('list_of_URLs.csv')
