import pandas as pd
import numpy as np
import os
import re
import urllib
import boto3
import botocore
from ast import literal_eval

def download_docs_to_IFS(head_path,files_df):
    for idx in files_df.index:
        print(str(idx+1) + ' out of ' + str(len(files_df)))
        destination_path = head_path + 'EDGAR/' + files_df.folder_name[idx] + '/'
        if not os.path.exists(destination_path): # make year folder if it doesn't already exist
            os.makedirs(destination_path)
        out_name = re.findall('/Archives/edgar/data/.*/.*/(.*)',files_df.document_url[idx])[0] # get the name of the file
        out_name = out_name.replace('html','htm') # problem with .html files here 
        urllib.urlretrieve(files_df.document_url[idx],destination_path + out_name) # download the file

def upload_files_to_S3(head_path,s3_client,s3_resource,df):
    for idx in df.index:
        # make appropriate S3 subdirectory
        folder_name = df.folder_name[idx]
        print(str(idx+1) + ' out of ' + str(len(df)))

        # see if directory already exists
        try: # does it exist?
            s3_resource.Object('gsb-mpjiang-s3', folder_name+'/').load()
        except botocore.exceptions.ClientError as e:
            response = s3_client.put_object(Bucket='gsb-mpjiang-s3', Body='', Key=folder_name+'/')
            print('   made directory')

        # upload photo to bucket
        print('      adding ' + df.photo_file[idx])
        s3_client.upload_file(head_path + 'EDGAR/' + folder_name + '/' + df.photo_file[idx], 'gsb-mpjiang-s3',\
                              folder_name+'/{}'.format(df.photo_file[idx]))
        
def in_tag(tmp,idx): # to make sure that the anchor is not placed inside of an HTML tag
    for ii,character in enumerate(tmp[idx:]):
        if character == '<': # in this case, the anchor will likely not land inside of an HTML tag
            return (False,idx)
        elif character == '>': # in this case, the anchor seems to be inside of an HTML tag
            idx = idx+ii+1 # revise the starting index
            return (True,idx)
        else:
            continue
    return (False,idx)

def pretag_htmls(head_path,s3_client,df,files_df):
    offset = 0
    for ii,file_name in enumerate(df.file_name.unique()[offset:]):
        # pick out subset of photos and files
        subset_photos = df[df.file_name == file_name].copy().reset_index(drop=True)
        subset_all_files = files_df[files_df.file_name == file_name].copy().reset_index(drop=True)

        # reference appropriate S3 subdirectory
        folder_name = subset_photos.folder_name[0]
        print(str(ii+offset) + ': working on ' + folder_name)

        # grab list of photos
        list_of_photos = list(subset_photos.photo_file)

        # find the appropriate html file
        list_of_docs = list(subset_all_files.document_url)
        for doc in list_of_docs:
            if (doc[-4:] == '.htm') or (doc[-4:] == 'html'): # if this is an html file
                html_name = re.findall('edgar/data/.*/.*/(.*)',doc)[0]
                break # have already checked that the first html file is always the proxy statement

        # open up def14a html file
        html_file = open(head_path + 'EDGAR/' + folder_name + '/' + html_name, 'r')
        the_text = html_file.read()

        # find the start of the photos...
        tags = []
        for photo_name in list_of_photos:
            print(photo_name)
            try:
                tags.append(re.search(photo_name,the_text).start())
            except:
                continue
        print(tags)
        if not tags:
            print('things didnt work out with this one...')
            continue
        elif len(tags) == 2:
            tag_idx = tags[-1]
        else:
            tag_idx = np.sort(tags)[int(np.ceil(np.median(range(1,len(tags)-1))))] # search for the middle photo tag

        print(tag_idx)
        # check whether the tag position is inside of a tag...
        (in_tag_flag,tag_idx) = in_tag(the_text,tag_idx)
        print(tag_idx)

        # tag the text
        tagged_txt = '<pre>' + the_text[:tag_idx] + \
        '</pre><a id="phototag"></a><pre>' + the_text[tag_idx:] + '</pre>'

        print('tag starts at ' + the_text[tag_idx-10:tag_idx+5])
        html_file.close()

        # save new, tagged html
        html_name_stripped = html_name.rstrip('.html').rstrip('.htm')
        tagged_file = open(head_path + 'EDGAR/' + folder_name + '/' + html_name_stripped + '_tagged.html','w')
        tagged_file.write(tagged_txt)
        tagged_file.close()

        # upload file to appropriate s3 bucket
        s3_client.upload_file(head_path + 'EDGAR/' + folder_name + '/' + html_name_stripped + '_tagged.html', 'gsb-mpjiang-s3', folder_name+'/{}'.format(html_name), ExtraArgs={'ContentType': "text/html"})
        
def make_google_forms(head_path,s3_client,df,files_df,j_script_path):
    with open(j_script_path, 'r') as j_file:
        j_script = j_file.read()
    j_file.close()

    offset = 0
    for ii,file_name in enumerate(df.file_name.unique()[offset:]):
        # pick out subset of photos and files
        subset_photos = df[df.file_name == file_name].copy().reset_index(drop=True)
        subset_all_files = files_df[files_df.file_name == file_name].copy().reset_index(drop=True)

        # reference appropriate S3 subdirectory
        folder_name = subset_photos.folder_name[0]
        print(str(ii+offset) + ': working on ' + folder_name)

        # generate pool of names
        name_id_pool = sorted(zip(literal_eval(subset_photos['director_names'][0]),literal_eval(subset_photos['executive_ids'][0])))

        # base script
        html_script = '<html>\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">\n' +\
        '<script data-cfasync="false" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>\n' +\
        '<title>Untitled Document</title>\n' +\
        '<style>\ntable, th, td {\nborder: 1px solid black;\nborder-collapse: collapse;\n}\n</style>\n' +\
        '</head>\n<body>\n<form id="foo">\n<p><ul class="columns" data-columns="2">'

        # grab list of photos
        list_of_photos = list(subset_photos.photo_file)

        # start table
        html_script = html_script + '</ul></p>\n<table style="width:50%">\n<tr>\n<th>Image</th>\n<th>Director Name</th>\n</tr>\n'

        # add in names
        drop_down_script = '<th><select name="director_name">'
        for name in name_id_pool:
            drop_down_script = drop_down_script + '<option value="' + name[0] + ';;' + str(name[1]) + '">' + name[0] + '</option>\n'
        drop_down_script = drop_down_script + '<option disabled="disabled">----</option>\n' +\
                                              '<option value="NOT A PHOTO OF A PERSON">NOT A PHOTO OF A PERSON</option>\n' +\
                                              '<option disabled="disabled">----</option>\n' +\
                                              '<option value="None" selected="selected">None</option></select>\n' +\
                                              '<br><br><p style="font-size:11px">If "None", please write in name:  ' +\
                                              '<input type="text" name="alt_name"></p></n></th>\n'

        # add in photos
        for ii,fname in enumerate(list_of_photos):
            html_script = html_script + '<tr>\n<th><img src="' + fname + '" style="width:120px;height:168px;"><input type="hidden" name="photo_file" value="' + fname + '"></th>\n' +\
            drop_down_script +\
            '<input type="hidden" name="file_name" value="' + file_name + '">\n</tr>\n'

        # drop-down select username
        html_script = html_script + '</table>\n' +\
        '<p>USERNAME: <select name="user_id">\n<option value="Narwhal">Narwhal</option>\n<option value="Rhino">Rhino</option>\n' +\
        '</select></p>\n' +\
        '<p id="result"></p>\n<input type="submit" value="Send"/>\n' +\
        '<input type="hidden" name="num_photos" value="' + str(len(list_of_photos)) + '">\n</form>\n'

        # add the Google Spreadsheet Script
        html_script = html_script + j_script + '\n</body>\n</html>'

        ## Write to html file and upload to S3
        template_path = head_path + 'EDGAR/' + folder_name + '/'
        f = open(template_path + folder_name + '_googleform_v1.html','w')
        print >>f, html_script
        f.close()
        s3_client.upload_file(template_path + folder_name + '_googleform_v1.html', 'gsb-mpjiang-s3',\
                              folder_name+'/{}'.format(folder_name+'_googleform_v1.html'), ExtraArgs={'ContentType': "text/html"})

def make_2frame_display(head_path,s3_client,df,files_df,base_address):
    offset = 0
    for ii,file_name in enumerate(df.file_name.unique()[offset:]):
        # pick out subset of photos and files
        subset_photos = df[df.file_name == file_name].copy().reset_index(drop=True)
        subset_all_files = files_df[files_df.file_name == file_name].copy().reset_index(drop=True)

        # reference appropriate S3 subdirectory
        folder_name = subset_photos.folder_name[0]
        print(str(ii+offset) + ': working on ' + folder_name)

        # base script
        html_script = '<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n' +\
        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\n<title>' + folder_name + '</title>\n' +\
        '<script src="/_res/js/all.js"></script>\n</head>\n<frameset cols="*,*" frameborder="no" border="0" framespacing="0">\n'

        # find the appropriate html file
        list_of_docs = list(subset_all_files.document_url)
        for doc in list_of_docs:
            if (doc[-4:] == '.htm') or (doc[-4:] == 'html'): # if this is an html file
                proxy_name = re.findall('edgar/data/.*/.*/(.*)',doc)[0]
                break # have already checked that the first html file is always the proxy statement

        # add document to left
        html_script = html_script + '<frame src="http://' + base_address + '/' + folder_name + '/' + proxy_name + '#phototag">\n'

        # add google form to right
        html_script = html_script + '<frame src="http://' + base_address + '/' + folder_name + '/' + folder_name + '_googleform_v1.html' + '">\n'

        # finish off script
        html_script = html_script + '</frameset>\n</html>'

        ## Write to html file
        template_path = head_path + 'EDGAR/' + folder_name + '/'
        f = open(template_path + folder_name + '_2frame_v1.html','w')
        print >>f, html_script
        f.close()
        s3_client.upload_file(template_path + folder_name + '_2frame_v1.html', 'gsb-mpjiang-s3',\
                              folder_name+'/{}'.format(folder_name+'_2frame_v1.html'), ExtraArgs={'ContentType': "text/html"})

