The main script to create the website is "make_S3_directors_website.py", although this is mostly for illustrative purposes as I built the website step-by-step in order to perform between-step testing.

The intermediate functions referenced in the main script are found in "s3_website_defs.py".

The 3 CSVs are as follows:
1. "docs_download_list.csv" was compiled from director_photo.filing_docs
2. "part3_master_list.csv" was compiled from director_photo.face_output and director_photo.filings_to_check
3. "list_of_URLs.csv" was made to fill in the Google Spreadsheet with objectives.

Finally, the "jquery_post_script.txt" contains the necessary code to funnel information from the S3 website to the Google Spreadsheet. This includes information relevant only to the specific Google Doc that we are working with. 
