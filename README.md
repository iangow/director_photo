# director_photo

The database schema includes the following tables:

- `ethnicities`: Cleaned ethnicity data by `executive_id`.
- `photo_matches`: Matches photos on EDGAR to directors on Equilar.
Based on data provided by Fiverr RA and `tagged_names` below.
- `filing_docs`: Table listing documents associated with every DEF 14A filing. *This probably belongs in the `filings` schema rather than here.*

- `hand_classification`: Data resulting from hand-classification of directors on which MTurkers disagreed.
- `filings_to_check`: A list of filings flagged for matching photos to directors.

- `mturk_data`: Results of classification of director images (with names) into gender and ethnicity.
- `checked_files`: Files examined by Fiverr RA in matching photos to directors.

- `rm_equilar`: Link table from Equilar to RiskMetrics data.
- `rm_ethnicities`: Ethnicity data from RiskMetrics.
